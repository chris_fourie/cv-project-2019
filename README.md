# CV Project 2019

Chris Fourie 358183 

## Setup Environment

Conda using environment.yml file

- `conda env create -f environment.yml`

  (This also has a number of superfluous packages in it, however if you are not too worried about space, it is really easy and comprehensive)

Otherwise use the requirements.txt for specific packages 

## Run Experiments

* Recommended to run using PyCharm as it was implemented as a PyCharm Project -> set source folder in project setting -> add 'data_generator', 'models' and 'utils' to ensure that custom packages will be discoverable 
* Open the Experiments folder
* `python run_data_generator.py`  will generate a training set of 3000 images and a testing set of 1000 images with corresponding keypoints 
* `python Exp1_Train_GmatchNet.py` will train the model 
    * Models will automatically be saved after every 50 samples, with information pertaining to the sample number and time 
* `python Exp1_Test_GmatchNet.py` will load selected models for testing 
    * The model file name will have to be specified in the python code  

## Tracking and Outputs 

* Public live results from experiments are tracked here https://www.comet.ml/chris-fourie/feature-matching-with-gcnns/view/
* Local outputs are provided, they include for each sample, an AUC score, batch loss and ROC curve graph. Every 10 samples an updated average ROC Curve with is produced with standard deviation over the current number of samples. 

## 'tests' folder 

This folder is rough work showing progress throughout the implementation and has been included to help capture the work flow for these kinds of projects. It can be useful for training, process communication as well as personal revision. 

To note the experiments contain no dependencies from the 'tests' folder. The name 'tests' is synonymous with rough work in that, it is were one just tests things / tries them out.  
