# https: // en.wikipedia.org / wiki / Affine_transformation
import numpy as np

def getTransformMatrix(difficulty):
    xs = 1 * difficulty
    ys = 1 * difficulty
    s1 = 1.2 * difficulty
    s2 = 1.2 * difficulty
    tx = 40 * difficulty
    ty = 40 * difficulty

    # set values
    a1 = np.random.uniform(0.8 * xs, 1 * xs, 1)  # X-axis scale (must be +'ve)
    a2 = np.random.uniform(-1 * s1, 1 * s1, 1)  # shear 1
    a3 = np.random.uniform(-1 * tx, 1 * tx, 1)  # X-axis translate 1
    b1 = np.random.uniform(-1 * s2, 1 * s2, 1)  # shear 2
    b2 = np.random.uniform(0.8 * ys, 1 * ys, 1)  # Y-axis scale (must be +'ve)
    b3 = np.random.uniform(-1 * ty, 1 * ty, 1)  # Y-axis translate
    c1 = 0
    c2 = 0
    c3 = 1

    # rotations
    theta = np.random.uniform(-3.147/6, 3.147/6, 1)

    a1 = np.multiply(np.cos(theta), a1)
    a2 = np.multiply(np.sin(theta), (-a2))
    b1 = np.multiply(np.sin(theta), b1)
    b2 = np.multiply(np.cos(theta), b2)

    perspM = np.array([[a1, a2, a3],
                      [b1, b2, b3],
                      [c1, c2, c3]], dtype=np.float32)

    affineM = np.array([[a1, a2, a3],
                        [b1, b2, b3]], dtype=np.float32)

    return perspM, affineM