import numpy as np

def makeLabelsFromPosIndexes(pi1, pi2, arr1, arr2):
    labels = np.zeros((len(arr1) * len(arr2)))
    for i in range(len(pi1)):
        labels[pi1[i] * pi2[i]] = 1

    return labels