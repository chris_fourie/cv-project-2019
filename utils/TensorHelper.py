import torch

def nodeRepresentationMerge (nodeRep1, nodeRep2):
    nodeRep1List = []
    nodeRep2List = []
    for i in range(len(nodeRep1)):
        for j in range(len(nodeRep2)):
            nodeRep1List.append(nodeRep1[i])
            nodeRep2List.append(nodeRep2[j])
    nodeRep1 = torch.cat(nodeRep1List)
    nodeRep2 = torch.cat(nodeRep2List)
    nodePairRep = torch.stack((nodeRep1, nodeRep2), 1)

    return nodePairRep



