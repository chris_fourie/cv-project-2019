import numpy as np
import math

def keypointToCoordinate(keypoints):
    pts = []
    for i in range(len(keypoints)):
        pts.append([keypoints[i].pt[0], keypoints[i].pt[1]])
    pts = np.asarray(pts)
    pts = np.rint(pts)
    return pts

def getCoordianteAngles(pts):
    # needs to be in an interleaved order to match edge ordering of graph, i.e. edges representing bidirectional edges are ordered next to one another
    angles = []
    for i in range(len(pts)):
        for j in range(len(pts)):
            # from node A -> B
            sourceAX = pts[i][0]
            sourceAY = pts[i][1]
            targetBX = pts[j][0]
            targetBY = pts[j][1]
            angles.append(math.atan2(targetBY - sourceAY, targetBX - sourceAX))

            # from node B -> A
            sourceBX = pts[i][0]
            sourceBY = pts[i][1]
            targetAX = pts[j][0]
            targetAY = pts[j][1]
            angles.append(math.atan2(targetAY - sourceBY, targetBX - sourceAX))

    angles = np.asarray(angles)
    np.reshape(angles, (-1, len(pts)))
    return angles