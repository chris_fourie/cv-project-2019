from data_generator import FeatureDetector
from utils import FeatureHelper
import cv2
import numpy as np

if __name__ == '__main__':
    # Hyperparameters

    imageFile = 'image2.png'

    FeatureDetector.orbDisplay('image2.png')
    img = cv2.imread('images/' + imageFile, 0)
    # Initiate
    orb = cv2.ORB_create()
    # find the keypoints with ORB
    keypoints = orb.detect(img, None)
    # compute the descriptors with ORB
    keypoints, descriptors = orb.compute(img, keypoints)

    print("Open CV ORB feature detector outputs ",len(keypoints), " kepoints")
    print("Each with ", descriptors.shape[1], " vector of integer descriptors")

    print('e.g.')
    print(keypoints[0])
    print(descriptors[0])

    print(type(keypoints[0]))
    print(keypoints[0].pt) # access x,y coordinate

    # convert list of cv keypoint objects to an np array of x,y coordinates
    pts = []
    for i in range (len(keypoints)):
        pts.append(keypoints[i].pt)
    print(pts)
    print(pts[0][0])
    pts = np.asarray(pts)
    print(pts)

    pts = FeatureHelper.keypointToCoordinate(keypoints)
    print(pts)
    






