import cv2
import torch
import torch.nn.functional as F
import dgl
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from torch import nn

from data_generator import FeatureDetector
from data_generator import generate
from models import GCNNs
from utils import FeatureHelper, TensorHelper
import sys


if __name__ == '__main__':
    from os.path import dirname
    sys.path.append(dirname(__file__))
    # hyperparameters
    scale = 0.7  # to down scale
    numTrain = 5  # number of training data
    numFeatures = 32 # number of features for each node

    # get coordinates
    imageFile1 = 'image2.png'
    FeatureDetector.orbDisplay(imageFile1)
    img1 = cv2.imread('images/' + imageFile1, 0)

    ######################
    # make training data #
    ######################
    trainingDataList = []
    for i in range(numTrain):
        pts1, pts2, descriptors1, descriptors2, labels, _, _ = generate.make(img1, scale)
        trainingDataList.append((pts1, pts2, descriptors1, descriptors2, labels))


    #################
    # SETUP NETWORK #
    #################

    # setup 2 graph neural networks
    D1_in = numFeatures
    D2_in = numFeatures
    H1 = 10  # hidden layer 1 size
    H2 = 5  # hidden layer 2 size
    D_out = 1  # output categories
    net1 = GCNNs.GCN(D1_in, H1, H2, D_out)
    net2 = GCNNs.GCN(D2_in, H1, H2, D_out)

    # setup non graph fully connected network for binary classification
    D_in, H, D_out = 2, 50, 1
    netClass = torch.nn.Sequential(
        torch.nn.Linear(D_in, H),
        torch.nn.ReLU(),
        torch.nn.Linear(H, D_out),
        torch.nn.Sigmoid())


    ###########
    ## TRAIN ##
    ###########
    criterion = nn.BCEWithLogitsLoss()  # MSE not great on sparse data
    optimizer = torch.optim.Adam(netClass.parameters(), lr=0.01)
    for epoch in range(5):
        for i in range(len(trainingDataList)):
            ## PREPARE DATA ##
            # unpack
            pts1, pts2, descriptors1, descriptors2, labels = trainingDataList[i]

            # points to tensor
            pts1 = torch.from_numpy(pts1)
            pts2 = torch.from_numpy(pts2)
            # descriptors / node features to tensor
            descriptors1 = torch.from_numpy(descriptors1)
            descriptors2 = torch.from_numpy(descriptors2)
            descriptors1 = descriptors1.type(torch.float)
            descriptors2 = descriptors2.type(torch.float)
            # reshape labels: from [-1, ] to [-1] to [-1, 1] # the empty dim is problematic sometimes need to squeeze and unsqueeze
            lables = labels.squeeze()
            labels = labels.unsqueeze(1)

            # tensor to kNN Graph
            g1 = dgl.knn_graph(pts1, 2)
            g2 = dgl.knn_graph(pts2, 2)

            ## FORWARD PASS 1 ##
            # get abstract, lower dimensional representation of nodes for each set of input with a forward pass
            nodeRep1 = net1(g1, descriptors1)
            nodeRep2 = net2(g2, descriptors2)
            # merge node representation
            nodePairRep = TensorHelper.nodeRepresentationMerge(nodeRep1, nodeRep2)

            ## FORWARD PASS 2 ##
            pred = netClass(nodePairRep)

            ## LOSS & BACKWARDS PASS
            # print(pred.shape)
            # print(labels.shape)
            loss = criterion(pred, labels)
            optimizer.zero_grad()
            loss.backward(retain_graph=True)  # seemingly neccessary for split graph structure  # https://stackoverflow.com/questions/46774641/what-does-the-parameter-retain-graph-mean-in-the-variables-backward-method # https://discuss.pytorch.org/t/runtimeerror-trying-to-backward-through-the-graph-a-second-time-but-the-buffers-have-already-been-freed-specify-retain-graph-true-when-calling-backward-the-first-time/6795
            optimizer.step()
            print('Epoch %d | Loss: %.4f' % (epoch, loss.item()))

        torch.set_printoptions(profile="default")
        print(pred)

        # TODO threshold pred for inspection
        # TODO add commet for graphing
        # TODO find a loss function for sparse labels
        ## https://pytorch.org/docs/stable/nn.html#torch.nn.NLLLoss
        # TODO save training data
        # TODO make testing data set




