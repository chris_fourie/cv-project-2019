from models import GCNNs as gnn
import networkx as nx
import torch
import torch.nn.functional as F

import matplotlib.animation as animation
import matplotlib.pyplot as plt

if __name__ == '__main__':
    # hyperparameters
    numNodes = 5
    numFeatures = 3

    g = gnn.buildRandomGraph(numNodes)
    print('We have %d nodes.' % g.number_of_nodes())
    print('We have %d edges.' % g.number_of_edges())

    nx_g = g.to_networkx().to_undirected()

    # Kamada-Kawaii layout usually looks pretty for arbitrary graphs
    pos = nx.kamada_kawai_layout(nx_g)
    nx.draw(nx_g, pos, with_labels=True, node_color=[[.7, .7, .7]])  # TODO why is this not drawing ??

    # assign features to nodes or edges
    ## assign a tensor with a 100 random values to each node
    g.ndata['id'] = torch.eye(numNodes)
    g.ndata['feat'] = torch.eye(numNodes)
    # g.ndata['feat'] = torch.rand((numNodes, numFeatures))
    ## print some nodes
    print(g.nodes[[1, 2, 3]].data['id'])
    print(g.nodes[[1, 2, 3]].data['feat'])

    # setup a graph neural network
    hl1 = 10  # hidden layer 1 size
    hl2 = 5  # hidden layer 2 size
    catOut = 2  # output categories  # what is this outputting ??
    net = gnn.GCN(len(g), hl1, hl2, catOut)  # len(g) returns number of nodes  # dgl._ffi.base.DGLError: Expect number of features to match number of nodes (len(u))

    # input preparation and initialization
    inputs = torch.eye(numNodes)
    # inputs = torch.rand(numNodes, numNodes)  # TODO understand what size the inputs have to be and why -> sqaure matrix of numNodes x numNodes - adjacency matrix?
    y = torch.rand((numNodes, 2))  #  labels
    print('inputs: \n', inputs)


    # train
    optimizer = torch.optim.Adam(net.parameters(), lr=0.01)
    all_logits = []
    for epoch in range(30):
        logits = net(g, inputs)
        all_logits.append(logits.detach())
        print('Output for iteration: ', epoch, '\n', logits)
        print(y)
        print('\n')
        loss = F.mse_loss(logits, y)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        print('Epoch %d | Loss: %.4f' % (epoch, loss.item()))



# def draw(i):
#     cls1color = '#00FFFF'
#     cls2color = '#FF00FF'
#     pos = {}
#     colors = []
#     for v in range(numNodes):
#         pos[v] = all_logits[i][v].numpy()
#         cls = pos[v].argmax()
#         colors.append(cls1color if cls else cls2color)
#     ax.cla()
#     ax.axis('off')
#     ax.set_title('Epoch: %d' % i)
#     nx.draw_networkx(nx_g.to_undirected(), pos, node_color=colors,
#             with_labels=True, node_size=300, ax=ax)
#
# fig = plt.figure(dpi=150)
# fig.clf()
# ax = fig.subplots()
# draw(0)  # draw the prediction of the first epoch
# plt.close()
#
# ani = animation.FuncAnimation(fig, draw, frames=len(all_logits), interval=200)
