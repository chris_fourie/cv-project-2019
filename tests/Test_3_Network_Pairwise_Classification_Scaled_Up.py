from models import GCNNs
import torch
import torch.nn.functional as F
from utils import TensorHelper

if __name__ == '__main__':
    # hyperparameters
    numNodes_1 = 3
    numNodes_2 = 4
    numFeatures = 5

    g1 = GCNNs.buildRandomGraph(numNodes_1)
    g2 = GCNNs.buildRandomGraph(numNodes_2)

    # assign features to nodes or edges
    ## assign each node a 1D tensor with numFeatures elements
    g1.ndata['feat'] = torch.rand((numNodes_1, numFeatures))
    g2.ndata['feat'] = torch.rand((numNodes_2, numFeatures))

    # setup 2 graph neural networks
    D_in = numFeatures
    H1 = 10  # hidden layer 1 size
    H2 = 5  # hidden layer 2 size
    D_out = 1  # output categories
    net1 = GCNNs.GCN(D_in, H1, H2, D_out)
    net2 = GCNNs.GCN(D_in, H1, H2, D_out)

    # input preparation and initialization for 2 non-identical GCNs
    inputs_1 = torch.rand(numNodes_1, numFeatures)  # input is numNodes x numFeatures
    inputs_2 = torch.rand(numNodes_2, numFeatures)
    # y = torch.rand((numNodes, 2))  #  labels
    # print('inputs: \n', inputs_1)

    # get abstract, lower dimensional representation of nodes for each set of input with a forward pass
    nodeRep1 = net1(g1, inputs_1)
    nodeRep2 = net2(g2, inputs_2)



    # merge node representation  # .stack() vs .cat() ?  # https://discuss.pytorch.org/t/torch-cat-torch-stack-which-one/40916
    nodePairRep = TensorHelper.nodeRepresentationMerge(nodeRep1, nodeRep2)
    print(nodePairRep)
    print(nodePairRep[1])

    # setup non graph fully connected network for binary classification
    D_in, H, D_out = 2, 50, 1
    netClass = torch.nn.Sequential(
        torch.nn.Linear(D_in, H),
        torch.nn.ReLU(),
        torch.nn.Linear(H, D_out),
        torch.nn.Sigmoid())

    # labels
    y = torch.randint(0, 2, (numNodes_1*numNodes_2, 1))  # node 1 to node A is true & node 2 to node B is true
    y = y.type(torch.FloatTensor)

    # train
    # optimizer = torch.optim.Adam(netClass.parameters(), lr=0.01)
    # for epoch in range(100):
    #     for i, nodePair in enumerate(nodePairRep):    # cycle through pair of nodes 2x3 -> 6 pairs
    #         y_pred = netClass(nodePair)
    #         loss = F.mse_loss(y_pred, y[i])
    #         optimizer.zero_grad()
    #         loss.backward(retain_graph=True)  #https://discuss.pytorch.org/t/runtimeerror-trying-to-backward-through-the-graph-a-second-time-but-the-buffers-have-already-been-freed-specify-retain-graph-true-when-calling-backward-the-first-time/6795
    #         optimizer.step()
    #
    #     print('Epoch %d | Loss: %.4f' % (epoch, loss.item()))
    #     print(y_pred)

    optimizer = torch.optim.Adam(netClass.parameters(), lr=0.01)
    for epoch in range(1000):
        y_pred = netClass(nodePairRep)
        loss = F.mse_loss(y_pred, y)
        optimizer.zero_grad()
        loss.backward(retain_graph=True) # seemingly neccessary for split graph structure  # https://stackoverflow.com/questions/46774641/what-does-the-parameter-retain-graph-mean-in-the-variables-backward-method # https://discuss.pytorch.org/t/runtimeerror-trying-to-backward-through-the-graph-a-second-time-but-the-buffers-have-already-been-freed-specify-retain-graph-true-when-calling-backward-the-first-time/6795
        optimizer.step()
        print('Epoch %d | Loss: %.4f' % (epoch, loss.item()))
    print (y)
    print(y_pred)

    print('Graph 1: %d nodes' % g1.number_of_nodes())
    print('Graph 1: %d edges' % g1.number_of_edges())
    print('Graph 2: %d nodes' % g2.number_of_nodes())
    print('Graph 2: %d edges' % g2.number_of_edges())
    print('Feature for each node: %d ' % numFeatures )
