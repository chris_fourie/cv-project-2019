import numpy as np
import cv2
from data_generator import FeatureDetector
from utils import FeatureHelper
import math

if __name__ == '__main__':
    # get coordinates
    imageFile = 'image2.png'
    FeatureDetector.orbDisplay('image2.png')
    img = cv2.imread('images/' + imageFile, 0)
    orb = cv2.ORB_create()
    keypoints = orb.detect(img, None)
    keypoints, descriptors = orb.compute(img, keypoints)
    pts = FeatureHelper.keypointToCoordinate(keypoints)

    # get angles between each set of points
    angles = []
    for i in range(len(pts)):
        for j in range(len(pts)):
            sourceX = pts[i][0]
            sourceY = pts[i][1]
            targetX = pts[j][0]
            targetY = pts[j][1]
            angles.append(math.atan2(targetY - sourceY, targetX - sourceX))

    angles = np.asarray(angles)
    angles = np.reshape(angles, (-1, len(pts)))
    print(angles)

    angles = FeatureHelper.getCoordianteAngles(pts)
    print(angles)
