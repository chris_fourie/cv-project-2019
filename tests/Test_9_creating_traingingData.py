import cv2
from data_generator import FeatureDetector
from data_generator.generate import make

if __name__ == '__main__':
    #hyperparameters
    scale = 0.7  # to down scale

    # get coordinates
    imageFile1 = 'image2.png'
    FeatureDetector.orbDisplay(imageFile1)
    img1 = cv2.imread('images/' + imageFile1, 0)

    pts1, pts2, descriptors1, descriptors2, labels = make(img1, scale)
    print(labels)

