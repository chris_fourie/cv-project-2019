import time

import numpy as np
import cv2
from data_generator import FeatureDetector
from models import GCNNs
from utils import FeatureHelper, TensorHelper, transformHelper
import torch
import math
import dgl
import networkx as nx
import matplotlib.pyplot as plt
import labelHelper

if __name__ == '__main__':
    #hyperparameters
    scale = 0.7  # to down scale
    torch.set_printoptions(profile="full")

    # get coordinates
    imageFile1 = 'image2.png'
    FeatureDetector.orbDisplay(imageFile1)
    img1 = cv2.imread('images/' + imageFile1, 0)
    # img1 = cv2.resize(img1, ((int)(img1.shape[1]*scale), (int)(img1.shape[0]*scale)))  # TODO downsample until there are <500 keypoints - deals with any size image -> can be used to average points
    orb = cv2.ORB_create()
    keypoints1, descriptors1 = orb.detectAndCompute(img1, None)

    # homography / affine transform
    perspM, affineM = transformHelper.getTransformMatrix()

    # apply transform from random matrix
    rows, cols = img1.shape
    img2 = cv2.warpPerspective(img1, perspM, (cols, rows))
    # img2 = cv2.warpAffine(img2, affineM, (cols, rows))

    # detect New Features on transformed image
    keypoints2, descriptors2 = orb.detectAndCompute(img2, None)
    img2NewFeatures = cv2.drawKeypoints(img2, keypoints2, img2, color=(100, 0, 200), flags=0)
    plt.imshow(img2NewFeatures), plt.show()

    # Display Old Features converted using homography transform
    # homography transformation for keypoints
    pts1 = FeatureHelper.keypointToCoordinate(keypoints1)
    pts1 = pts1.reshape(1, -1, 2)  # reshape for tranform input
    pts1 = cv2.perspectiveTransform(pts1, perspM)
    # pts1 = cv2.transform(pts1, affineM)

    pts1 = pts1.reshape(-1, 2)
    keyPoints1Draw = []  # reset keypoints1
    for i in range(len(pts1)):
        x = pts1[i, 0]
        y = pts1[i, 1]
        keyPoints1Draw.append(cv2.KeyPoint(x, y, 1)) # Note don't convert / sample from self created keypoints only good for drawing

    img3 = cv2.warpPerspective(img1, perspM, (cols, rows))
    # img3 = cv2.warpAffine(img1, affineM, (cols, rows))
    img2OldFeatures = cv2.drawKeypoints(img3, keyPoints1Draw, img3, color=(0, 100, 200), flags=0)
    plt.imshow(img2OldFeatures), plt.show()

    #############

    # Determine which are noise and which are  - that is stable Features Between both images
    # 1n cycle through one set - see if it exits in the other
    # does set B contain element from set A

    # take the intersection of two arrays
    # already have pts1
    pts2 = FeatureHelper.keypointToCoordinate(keypoints2)

    # round to integers
    pts1 = np.rint(pts1)
    pts2 = np.rint(pts2)

    # apply set operation
    a = set((tuple(i) for i in pts1))
    b = set((tuple(i) for i in pts2))
    c = a.intersection(b)  # positive labels
    d1 = a.difference(b)  # negative labels
    d2 = b.difference(a)

    positiveLabelValues = np.array(list(c))
    negativeLabelValues1 = np.array((list(d1)))
    negativeLabelValues2 = np.array((list(d2)))

    # find corresponding indexes to values in both sets of points
    positiveLabelIndexesList1 = []
    for i in range(len(positiveLabelValues)):
        cond1 = np.equal(pts1[:, 0], positiveLabelValues[i, 0])
        cond2 = np.equal(pts1[:, 1], positiveLabelValues[i, 1])
        cond3 = np.logical_and(cond1, cond2)
        positiveLabelIndexesList1.append(np.where(cond3)[0].item(0))
    positiveLabelIndexes1 = np.asarray(positiveLabelIndexesList1)

    positiveLabelIndexesList2 = []
    for i in range(len(positiveLabelValues)):
        cond1 = np.equal(pts2[:, 0], positiveLabelValues[i, 0])
        cond2 = np.equal(pts2[:, 1], positiveLabelValues[i, 1])
        cond3 = np.logical_and(cond1, cond2)
        positiveLabelIndexesList2.append(np.where(cond3)[0].item(0))
    positiveLabelIndexes2 = np.asarray(positiveLabelIndexesList2)

    negativeLabelIndexesList1 = []
    for i in range(len(negativeLabelValues1)):
        cond1 = np.equal(pts1[:, 0], negativeLabelValues1[i, 0])
        cond2 = np.equal(pts1[:, 1], negativeLabelValues1[i, 1])
        cond3 = np.logical_and(cond1, cond2)
        negativeLabelIndexesList1.append(np.where(cond3)[0].item(0))
    negativeLabelIndexes1 = np.asarray(negativeLabelIndexesList1)

    negativeLabelIndexesList2 = []
    for i in range(len(negativeLabelValues2)):
        cond1 = np.equal(pts2[:, 0], negativeLabelValues2[i, 0])
        cond2 = np.equal(pts2[:, 1], negativeLabelValues2[i, 1])
        cond3 = np.logical_and(cond1, cond2)
        negativeLabelIndexesList2.append(np.where(cond3)[0].item(0))
    negativeLabelIndexes2 = np.asarray(negativeLabelIndexesList2)


    print("POS - intersection")
    print(positiveLabelIndexes1)
    print(positiveLabelValues.shape)
    print("")
    print("NEG")
    print("Set A - Set B")
    print(negativeLabelIndexes1)
    print(negativeLabelValues1.shape)
    print("")
    print("Set B - Set A")
    print(negativeLabelIndexes2)
    print(negativeLabelValues2.shape)

    positiveKeyPointsDraw1 = []  # as it is an intersection should only have to draw 1
    for i in range(len(positiveLabelIndexes1)):
        x = pts1[positiveLabelIndexes1[i], 0]
        y = pts1[positiveLabelIndexes1[i], 1]
        positiveKeyPointsDraw1.append(cv2.KeyPoint(x, y, 1))

    negativeKeyPointsDraw1 = []
    for i in range(len(negativeLabelIndexes1)):
        x = pts1[negativeLabelIndexes1[i], 0]
        y = pts1[negativeLabelIndexes1[i], 1]
        negativeKeyPointsDraw1.append(cv2.KeyPoint(x, y, 1))

    negativeKeyPointsDraw2 = []
    for i in range(len(negativeLabelIndexes2)):
        x = pts2[negativeLabelIndexes2[i], 0]
        y = pts2[negativeLabelIndexes2[i], 1]
        negativeKeyPointsDraw2.append(cv2.KeyPoint(x, y, 1))

    img4 = cv2.warpPerspective(img1, perspM, (cols, rows))
    # img4 = cv2.warpAffine(img1, affineM, (cols, rows))
    imgFeatureLabels = cv2.drawKeypoints(img4, negativeKeyPointsDraw1, img4, color=(255, 0, 0), flags=0)  # red
    imgFeatureLabels = cv2.drawKeypoints(imgFeatureLabels, negativeKeyPointsDraw2, imgFeatureLabels, color=(255, 0, 0), flags=0)  # red
    imgFeatureLabels = cv2.drawKeypoints(imgFeatureLabels, positiveKeyPointsDraw1, imgFeatureLabels, color=(0, 255, 0), flags=0)  # green
    plt.imshow(imgFeatureLabels), plt.show()



    labels = labelHelper.makeLabelsFromPosIndexes(positiveLabelIndexes1, positiveLabelIndexes2, pts1, pts2)

    print(labels.shape)

    sumPosLabels = 0
    for i in range(len(labels)):
        if (labels[i] == 1):
            sumPosLabels += 1
    print(sumPosLabels)


    # convert to binary vector for node-pair label
    # merge

    # at index positive = 1 at index negative = 0  for both
    # then merge
    ####################################





    ###################################
    ## BROKEN ##
    # pts1Label = np.zeros(len(pts1))
    # # for i in range(len(positiveLabelIndexes1)):
    # #     pts1Label[positiveLabelIndexes1[i]] = 1
    # # for i in range(len(negativeLabelIndexes1)):
    # #     pts1Label[negativeLabelIndexes1[i]] = 0
    #
    # pts2Label = np.zeros(len(pts2))
    # # for i in range(len(positiveLabelIndexes2)):
    # #     pts2Label[positiveLabelIndexes2[i]] = 1
    # # for i in range(len(negativeLabelIndexes2)):
    # #     pts2Label[negativeLabelIndexes2[i]] = 0
    #
    # #######################################
    #
    # #convert to torch tensors
    # pst1Label = pts1Label.reshape(-1, 1)
    # pst2Label = pts2Label.reshape(-1, 1)
    # # pst1Label = torch.from_numpy(pst1Label).type(torch.uint8)
    # # pst2Label = torch.from_numpy(pst2Label).type(torch.uint8)
    #
    # ## probe
    # sumPosLabels = 0
    # for i in range(len(pts1)):
    #     if (pts1Label[i] == 1):
    #         sumPosLabels += 1
    # print(sumPosLabels)
    #
    # sumPosLabels = 0
    # for i in range(len(pts1)):
    #     if (pts2Label[i] == 1):
    #         sumPosLabels += 1
    # print(sumPosLabels)
    #
    # print()
    #
    # print(pts1Label[10])
    #
    # # labelPair = TensorHelper.nodeRepresentationMerge(pst1Label, pst2Label)
    # labelPair = TensorHelper.nodeRepresentationMergeNp(pst1Label, pst2Label)
    #
    #
    # print(labelPair)
    # print(labelPair.shape)
    #
    # ## probe
    # sumPosLabels = 0
    # for i in range(len(pts1)):
    #     if (labelPair[i,0] == 1):
    #         sumPosLabels += 1
    # print(sumPosLabels)
    #
    #
    # ## probe
    # sumPosLabels = 0
    # for i in range(len(pts1)):
    #     if (labelPair[i, 1] == 1):
    #         sumPosLabels += 1
    # print(sumPosLabels)
    #
    # #labels
    # # labelsLen = len(pts1)*len(pts2)
    # # labels = torch.zeros(len(labelPair))
    # labels = np.zeros((len(labelPair)))
    # for i in range(len(labelPair)):
    #     if (labelPair[i,0] == 1 and labelPair[i,1] == 1):
    #         labels[i] = 1.0

    # print(labels)
    # print(labels.shape)

    # # ## probe
    # sumPosLabels = 0
    # for i in range(len(labelPair)):
    #     if (labels[i] == 1):
    #         sumPosLabels += 1
    # print(sumPosLabels)
    #
    # print(labels.shape)







