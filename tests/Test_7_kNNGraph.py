import numpy as np
import cv2
from data_generator import FeatureDetector
from models import GCNNs
from utils import FeatureHelper, TensorHelper
import torch
import math
import dgl
import networkx as nx
import matplotlib.pyplot as plt

if __name__ == '__main__':
    # get coordinates
    imageFile1 = 'image2.png'
    imageFile2 = 'image1.png'

    FeatureDetector.orbDisplay(imageFile1)
    FeatureDetector.orbDisplay(imageFile2)

    img1 = cv2.imread('images/' + imageFile1, 0)
    img2 = cv2.imread('images/' + imageFile2, 0)

    orb = cv2.ORB_create()

    keypoints1 = orb.detect(img1, None)
    print(len(keypoints1))
    keypoints2 = orb.detect(img2, None)
    print(len(keypoints2))

    keypoints1, descriptors1 = orb.compute(img1, keypoints1)
    keypoints2, descriptors2 = orb.compute(img2, keypoints2)

    pts1 = FeatureHelper.keypointToCoordinate(keypoints1)
    print(len(pts1))
    pts2 = FeatureHelper.keypointToCoordinate(keypoints2)  # TODO why so few points ?
    print(len(pts2))

    # points to tensor
    x1 = torch.from_numpy(pts1)
    x2 = torch.from_numpy(pts2)
    # tensor to kNN Graph
    g1 = dgl.knn_graph(x1, 2)
    g2 = dgl.knn_graph(x2, 2)

    # visualize graph
    nx_g = g1.to_networkx().to_undirected()

    # Kamada-Kawaii layout usually looks pretty for arbitrary graphs
    pos = nx.kamada_kawai_layout(nx_g)
    nx.draw_spring(nx_g, with_labels=True, node_color=[[.7, .7, .7]])
    plt.show()

    print("g1 number of nodes: ", g1.number_of_nodes())
    print("g2 number of nodes: ", g2.number_of_nodes())


    # get angles between each set of points
    angles1 = FeatureHelper.getCoordianteAngles(pts1)
    angles2 = FeatureHelper.getCoordianteAngles(pts2)

    #TODO should get distances as well

    # add node(keypoints, descriptors), edge(angles, *distances*) to to graph
    # hyperparameters
    nodeFeatures1 = descriptors1
    nodeFeatures2 = descriptors2
    edgeFeatures1 = angles1
    edgeFeatures2 = angles2
    numNodes_1 = len(keypoints1)
    numNodes_2 = len(keypoints2)
    numNodeFeatures1 = nodeFeatures1[1].shape[0]  # returns 32
    numNodeFeatures2 = nodeFeatures2[1].shape[0]  # returns 32
    numEdgeFeatures1 = len(edgeFeatures1)
    numEdgeFeatures2 = len(edgeFeatures2)

    # g1 = Networks.buildFCGraph(numNodes_1)
    # g2 = Networks.buildFCGraph(numNodes_2)

    # assign features to nodes or edges
    ## assign each node a 1D tensor with numNodeFeatures elements
    g1.ndata['feat'] = descriptors1
    g2.ndata['feat'] = descriptors2
    ## assign each edge a 1D tensor with numEdgeFeatures elements
    u = np.arange(0, 500)
    v = np.arange(0, 500)
    # TODO make sure that when assigning edge feature, that the right edges get the right features
    # TODO edges are stored in an interleaved order it seems
    # print(g1.edge_ids(u, v))
    # print(g1.edge_ids(u, v).size())
    # g1.edata['feat'] = angles1
    # g2.edata['feat'] = angles2

    # setup 2 graph neural networks
    D1_in = numNodeFeatures1
    D2_in = numNodeFeatures2
    H1 = 10  # hidden layer 1 size
    H2 = 5  # hidden layer 2 size
    D_out = 1  # output categories
    net1 = GCNNs.GCN(D1_in, H1, H2, D_out)
    net2 = GCNNs.GCN(D2_in, H1, H2, D_out)


    # input preparation and initialization for 2 non-identical GCNs
    inputs_1 = torch.rand(numNodes_1, numNodeFeatures1)  # input is numNodes x numFeatures
    inputs_2 = torch.rand(numNodes_2, numNodeFeatures2)
    # y = torch.rand((numNodes, 2))  #  labels
    # print('inputs: \n', inputs_1)

    # get abstract, lower dimensional representation of nodes for each set of input with a forward pass
    nodeRep1 = net1(g1, inputs_1)
    nodeRep2 = net2(g2, inputs_2)

    # merge node representation  # .stack() vs .cat() ?  # https://discuss.pytorch.org/t/torch-cat-torch-stack-which-one/40916
    nodePairRep = TensorHelper.nodeRepresentationMerge(nodeRep1, nodeRep2)

    # torch.set_printoptions(profile="full")
    print(nodePairRep)
    print(nodePairRep[1])

    print(len(nodePairRep))

