import pickle
import cv2
import matplotlib.pyplot as plt

from data_generator import dataSaver
from utils import transformHelper, FeatureHelper

if __name__ == '__main__':

    # dataSaver.saveTrainData(3,"pop")

    with open('training_data_2019-11-20 16:16:54.969348.pkl', 'rb') as fp:
        itemlist = pickle.load(fp)

    for item in itemlist:
        pnts1, pnts2, descriptor1, descriptor2, labels, img1, img2 = item

        # print(item)

        orb = cv2.ORB_create()
        keypoints1, descriptors1 = orb.detectAndCompute(img1, None)

        # homography / affine transform
        perspM, affineM = transformHelper.getTransformMatrix()

        # apply transform from random matrix
        rows, cols = img1.shape
        img2 = cv2.warpPerspective(img1, perspM, (cols, rows))
        # img2 = cv2.warpAffine(img2, affineM, (cols, rows))

        # detect New Features on transformed image
        keypoints2, descriptors2 = orb.detectAndCompute(img2, None)
        img2NewFeatures = cv2.drawKeypoints(img2, keypoints2, img2, color=(0, 0, 255), flags=0)
        plt.imshow(img2NewFeatures), plt.show()

        # Display Old Features converted using homography transform
        # homography transformation for keypoints
        pts1 = FeatureHelper.keypointToCoordinate(keypoints1)
        pts1 = pts1.reshape(1, -1, 2)  # reshape for tranform input
        pts1 = cv2.perspectiveTransform(pts1, perspM)
        # pts1 = cv2.transform(pts1, affineM)

        pts1 = pts1.reshape(-1, 2)
        keyPoints1Draw = []  # reset keypoints1
        for i in range(len(pts1)):
            x = pts1[i, 0]
            y = pts1[i, 1]
            keyPoints1Draw.append(
                cv2.KeyPoint(x, y, 1))  # Note don't convert / sample from self created keypoints only good for drawing

        img3 = cv2.warpPerspective(img1, perspM, (cols, rows))
        # img3 = cv2.warpAffine(img1, affineM, (cols, rows))
        img2OldFeatures = cv2.drawKeypoints(img3, keyPoints1Draw, img3, color=(0, 0, 255), flags=0)
        plt.imshow(img2OldFeatures), plt.show()




