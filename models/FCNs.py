import torch

def fcn1(D_in, H, D_out):
    netClass = torch.nn.Sequential(
        torch.nn.Linear(D_in, H),
        torch.nn.ReLU(),
        torch.nn.Linear(H, D_out),
        torch.nn.Sigmoid())

    return netClass


def fcn2(D_in, H, D_out):
    netClass = torch.nn.Sequential(
        torch.nn.Linear(D_in, H),
        torch.nn.ReLU(),
        torch.nn.Linear(H, D_out))

    return netClass