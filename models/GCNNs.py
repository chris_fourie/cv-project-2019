import dgl
import torch
import torch.nn as nn
import random
import torch.nn.functional as F
import dgl.nn.pytorch  # TODO bug -> unecessary import


# Define the message and reduce function
# NOTE: We ignore the GCN's normalization constant c_ij
def gcn_message(edges):
    # The argument is a batch of edges.
    # This computes a (batch of) message called 'msg' using the source node's feature 'h'.
    return {'msg': edges.src['h']}


def gcn_reduce(nodes):
    # The argument is a batch of nodes.
    # This computes the new 'h' features by summing received 'msg' in each node's mailbox.
    return {'h': torch.sum(nodes.mailbox['msg'], dim=1)}


# Define the GCNLayer module
class GCNLayer(nn.Module):
    def __init__(self, in_feats, out_feats):
        super(GCNLayer, self).__init__()
        self.linear = nn.Linear(in_feats, out_feats)

    def forward(self, g, inputs):
        # g is the graph and the inputs are the input node features
        # first set the node features
        g.ndata['h'] = inputs
        # trigger message passing on all edges
        g.send(g.edges(), gcn_message)
        # trigger aggregation at all nodes
        g.recv(g.nodes(), gcn_reduce)
        # get the result node features
        h = g.ndata.pop('h')
        # perform linear transformation
        return self.linear(h)


# Define a 2-layer GCN model
class GCN_2(nn.Module):
    def __init__(self, in_feats, hidden_size_1, D_out):  # in_feats = numFeatures
        super(GCN_2, self).__init__()
        self.gcn1 = GCNLayer(in_feats, hidden_size_1)
        self.gcn2 = GCNLayer(hidden_size_1, D_out)

    def forward(self, g, inputs):
        h = self.gcn1(g, inputs)
        h = torch.relu(h)
        h = self.gcn2(g, h)
        h = torch.relu(h)
        return h

# 3 Layer GCN
class GCN_3(nn.Module):
    def __init__(self, in_feats, hidden_size_1, hidden_size_2, D_out):  # in_feats = numFeatures
        super(GCN_3, self).__init__()
        self.gcn1 = GCNLayer(in_feats, hidden_size_1)
        self.gcn2 = GCNLayer(hidden_size_1, hidden_size_2)
        self.gcn3 = GCNLayer(hidden_size_2, D_out)

    def forward(self, g, inputs):
        h = self.gcn1(g, inputs)
        h = torch.relu(h)
        h = self.gcn2(g, h)
        h = torch.relu(h)
        h = self.gcn3(g, h)
        h = torch.relu(h)
        return h


# GMatchNet1
class GMatchNet1(nn.Module):
    def __init__(self, in_feats, D_out):  # in_feats = numFeatures
        super(GMatchNet1, self).__init__()
        self.gcn0 = GCNLayer(in_feats, 100)
        # self.maxP0 = dgl.nn.pytorch.glob.MaxPooling()
        self.gcn1 = GCNLayer(100, 50)
        # self.maxP1 = dgl.nn.pytorch.glob.MaxPooling()
        self.gcn2 = GCNLayer(50, 40)
        self.gcn3 = GCNLayer(40, 20)
        self.gcn4 = GCNLayer(20, 20)
        # self.maxP4 = dgl.nn.pytorch.glob.MaxPooling()
        self.bottleneck = self.gcn4 = GCNLayer(20, D_out)

    def forward(self, g, inputs):
        h = self.gcn0(g, inputs)
        h = torch.relu(h)
        # h = self.maxP0(g, h)
        h = self.gcn1(g, h)
        h = torch.relu(h)
        # h = self.maxP1(g, h)
        h = self.gcn2(g, h)
        h = torch.relu(h)
        h = self.gcn3(g, h)
        h = torch.relu(h)
        # h = self.gcn4(g, h)
        # h = torch.relu(h)
        # h = self.maxP4(g, h)
        h = self.bottleneck(g, h)
        h = torch.relu(h)
        return h


# GMatchNet2
class GMatchNet2(nn.Module):
    def __init__(self, in_feats, D_out):  # in_feats = numFeatures
        super(GMatchNet2, self).__init__()
        self.gcn0 = dgl.nn.pytorch.conv.GraphConv(in_feats, 100, norm=False)
        self.maxP0 = dgl.nn.pytorch.glob.MaxPooling()
        self.gcn1 = dgl.nn.pytorch.conv.GraphConv(100, 50, norm=False)
        self.maxP1 = dgl.nn.pytorch.glob.MaxPooling()
        self.gcn2 = dgl.nn.pytorch.conv.GraphConv(50, 40, norm=False)
        self.gcn3 = dgl.nn.pytorch.conv.GraphConv(40, 30, norm=False)
        self.gcn4 = dgl.nn.pytorch.conv.GraphConv(30, 20, norm=False)
        self.maxP4 = dgl.nn.pytorch.glob.MaxPooling()
        self.bottleneck = self.gcn4 = dgl.nn.pytorch.conv.GraphConv(20, D_out, norm=False)

    def forward(self, g, inputs):
        h = self.gcn0(g, inputs)
        h = torch.relu(h)
        h = self.maxP0(g, h)
        h = self.gcn1(g, h)
        h = torch.relu(h)
        h = self.maxP1(g, h)
        h = self.gcn2(g, h)
        h = torch.relu(h)
        h = self.gcn3(g, h)
        h = torch.relu(h)
        h = self.gcn4(g, h)
        h = torch.relu(h)
        h = self.maxP4(g, h)
        h = self.bottleneck(g, h)
        h = torch.relu(h)
        return h


def buildFCGraph(numNodes):  # fully connected graph did not learn 'well' and gave 'boring' out puts
    edge_list = []
    g = dgl.DGLGraph()

    # generate nodes
    g.add_nodes(numNodes)

    # connect all nodes to each other
    for i in range(numNodes):
        for j in range(numNodes):
            edge_list.append((i, j))

    # add edges two lists of nodes: src and dst
    src, dst = tuple(zip(*edge_list))
    g.add_edges(src, dst)
    # edges are directional in DGL; make them bi-directional
    g.add_edges(dst, src)

    return g




def buildRandomGraph(numNodes):  # fully connected graph did not learn 'well' and gave 'boring' out puts
    edge_list = []
    g = dgl.DGLGraph()

    # generate nodes
    g.add_nodes(numNodes)

    # connect nodes
    for i in range(numNodes):
        for j in range(numNodes):
            edge_list.append((random.randint(0, numNodes - 1), (random.randint(0, numNodes - 1))))

    # add edges two lists of nodes: src and dst
    src, dst = tuple(zip(*edge_list))
    g.add_edges(src, dst)
    # edges are directional in DGL; make them bi-directional
    g.add_edges(dst, src)

    return g