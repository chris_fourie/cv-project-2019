import numpy as np
import cv2
from utils import FeatureHelper, transformHelper, labelHelper


def make (img1, scale):
    # Hyperparameters
    # Scale to down sample image
    orb = cv2.ORB_create()


    ######################
    # IMAGE & POINTS 1   #
    ######################
    # Image Pre-processing #
    #######################
    # Down sample
    scale = 1
    keypoints1 = [None] * 501
    while (len(keypoints1) > 499):
        img1 = cv2.resize(img1, ((int)(img1.shape[1]*scale), (int)(img1.shape[0]*scale)))  # downsample until there are <500 keypoints - deals with any size image -> can be used to average points
        keypoints1, descriptors1 = orb.detectAndCompute(img1, None)
        scale -= 0.1  # decrement the size of the image


    ####################
    # IMAGE & POINTS 2 #
    ####################
    # homography / affine transform
    perspM, affineM = transformHelper.getTransformMatrix(1)
    rows, cols = img1.shape
    img2 = cv2.warpPerspective(img1, perspM, (cols, rows))
    # detect New Features on transformed image
    keypoints2, descriptors2 = orb.detectAndCompute(img2, None)


    #######################
    # POINTS 1 TRANSFORM  #
    #######################
    # Transform Old Features converted using homography
    pts1 = FeatureHelper.keypointToCoordinate(keypoints1)
    pts1 = pts1.reshape(1, -1, 2)  # reshape for transform input
    pts1 = cv2.perspectiveTransform(pts1, perspM)
    pts1 = pts1.reshape(-1, 2)


    ###########################
    # INTERSECTION OF POINTS #
    ##########################
    # Determine which are noise and which are  - that is stable Features Between both images
    # does set B contain element from set A

    # take the intersection of two arrays
    ### already have pts1 ###
    pts2 = FeatureHelper.keypointToCoordinate(keypoints2)

    # round to integers
    pts1 = np.rint(pts1)
    pts2 = np.rint(pts2)

    # apply set operation
    a = set((tuple(i) for i in pts1))
    b = set((tuple(i) for i in pts2))
    c = a.intersection(b)  # positive labels

    positiveLabelValues = np.array(list(c))  # intersection represents both sets of points

    # find corresponding indexes to values in both sets of points
    positiveLabelIndexesList1 = []
    for i in range(len(positiveLabelValues)):
        cond1 = np.equal(pts1[:, 0], positiveLabelValues[i, 0])
        cond2 = np.equal(pts1[:, 1], positiveLabelValues[i, 1])
        cond3 = np.logical_and(cond1, cond2)
        positiveLabelIndexesList1.append(np.where(cond3)[0].item(0))
    positiveLabelIndexes1 = np.asarray(positiveLabelIndexesList1)

    positiveLabelIndexesList2 = []
    for i in range(len(positiveLabelValues)):
        cond1 = np.equal(pts2[:, 0], positiveLabelValues[i, 0])
        cond2 = np.equal(pts2[:, 1], positiveLabelValues[i, 1])
        cond3 = np.logical_and(cond1, cond2)
        positiveLabelIndexesList2.append(np.where(cond3)[0].item(0))
    positiveLabelIndexes2 = np.asarray(positiveLabelIndexesList2)

    ## use the positive indexes to set the positive pairs -> for i in  posIndexes set labels[posI1, posI2] = 1
    labels = labelHelper.makeLabelsFromPosIndexes(positiveLabelIndexes1, positiveLabelIndexes2, pts1, pts2)

    ## FOR PREDICTION: return 2 sets of points to be converted to graphs for pair wise classification
    ## FOR LOSS: return labels to calculate loss
    return pts1, pts2, descriptors1, descriptors2, labels, img1, img2








