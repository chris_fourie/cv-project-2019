# reference: https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_feature2d/py_orb/py_orb.html#orb

import cv2
from matplotlib import pyplot as plt

def orbDisplay(imageFile):
    img = cv2.imread('images/' + imageFile, 0)

    # Initiate STAR detector
    orb = cv2.ORB_create()

    # find the keypoints with ORB
    kp = orb.detect(img, None)

    # compute the descriptors with ORB
    kp, des = orb.compute(img, kp)

    # draw only keypoints location,not size and orientation
    img2 = img
    img2 = cv2.drawKeypoints(img, kp, img2, color=(0, 100, 200), flags=0)
    plt.imshow(img2), plt.show()
