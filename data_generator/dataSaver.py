import cv2
import pickle
import datetime as time
from data_generator import generate

def saveData(numImg, numSample, imgPATH, option):
    # hyperparameters
    scale = 0.7 # down scale image
    numSample = numSample  # number of training data
    numImg = numImg
    trainingDataList = []


    for j in range(numImg):
        imageFile1 = 'image' + (str)(j) + '.png'
        img1 = cv2.imread(imgPATH + imageFile1, 0)

        ######################
        # make training data #
        ######################

        for i in range(numSample):
            pts1, pts2, descriptors1, descriptors2, labels, img1, img2 = generate.make(img1, scale)
            trainingDataList.append((pts1, pts2, descriptors1, descriptors2, labels, img1, img2))
            print("img: ",j , "  version: ", i)

    ######################
    # save training data # https://stackoverflow.com/questions/25464295/how-to-pickle-a-list
    ######################
    # with open((str)(option) + '_data_Im'+ (str)(numImg) +'_S' + (str)(numSample)+'_'+ (str)(time.datetime.now()) + '.pkl', 'wb') as f:
    with open('../Experiments/'+(str)(option) + '_data_Im' + (str)(numImg) + '_S' + (str)(numSample) + '.pkl', 'wb') as f:
        pickle.dump(trainingDataList, f)

def saveTrainData(numImg, numSample):
    imgPATH = 'training_set/'
    saveData(numImg, numSample, imgPATH, 'training')

def saveTestData(numImg, numSample):
    imgPATH = 'testing_set/'
    saveData(numImg, numSample, imgPATH, 'testing')




