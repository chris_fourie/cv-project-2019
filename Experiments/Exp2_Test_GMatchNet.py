import csv

from comet_ml import Experiment

experiment = Experiment(api_key="YgEw8QI0yb6BFSogbIXBvw2i5",
                        project_name="feature-matching-with-gcnns", workspace="chris-fourie")

import pickle
import torch
from torch import nn
import dgl
import datetime as time

import numpy as np

from utils import additionalMetrics

from models import GCNNs, FCNs
from utils import TensorHelper
import pandas as pd
import seaborn as sn

from scipy import interp
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc, roc_auc_score
from sklearn.model_selection import StratifiedKFold



if __name__ == '__main__':
    # Device configuration
    # device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    # Experiment tracking
    experiment.log_code = True
    experiment.log_graph = True
    experiment.log_env_details = True
    experiment.auto_param_logging = True
    # experiment.auto_metric_logging = True
    experiment.auto_output_logging = True

    ########################
    #  Set Hyperparameters #
    ########################
    # hyperparameters
    dataPATH = 'testing_data_Im10_S100.pkl'
    net1PATH = 'net1.pt'
    net2PATH = 'net2.pt'
    netClassPATH = 'netClass.pt'
    k = 3

    numFeatures = 32  # number of features for each node
    numEpochs = 2

    #################
    # SETUP NETWORK #
    #################
    # setup 2 graph neural networks
    D1_in, D2_in = numFeatures, numFeatures
    D_out = 1  # output categories  # consider using this as a hyperparameter
    net1 = GCNNs.GMatchNet1(D1_in, D_out)
    net2 = GCNNs.GMatchNet1(D2_in, D_out)

    # setup non graph fully connected network for binary classification
    D_in, H, D_out = 2, 50, 1
    netClass = FCNs.fcn2(D_in, H, D_out)

    #############
    # LOAD DATA #
    #############
    with open(dataPATH, 'rb') as fp:
        dataList = pickle.load(fp)

        ###############
        # LOAD MODELS #
        ###############
        net1.load_state_dict(torch.load(net1PATH))
        net2.load_state_dict(torch.load(net2PATH))
        netClass.load_state_dict(torch.load(netClassPATH))

        net1.eval()
        net2.eval()
        netClass.eval()


        ###########
        ## TEST  ##
        ###########
        count = 1
        total_loss = 0
        criterion = nn.BCEWithLogitsLoss()  # MSE not great on sparse data

        ## for ROC Variance ##
        tprs = []
        aucs = []
        mean_fpr = np.linspace(0, 1, 100)

        for epoch in range(numEpochs):
            for data in dataList:
                ## PREPARE DATA ##
                # unpack data
                pts1, pts2, descriptors1, descriptors2, labels, _, _ = data

                # points to tensor
                pts1 = torch.from_numpy(pts1)
                pts2 = torch.from_numpy(pts2)
                # descriptors / node features to tensor
                descriptors1 = torch.from_numpy(descriptors1)
                descriptors2 = torch.from_numpy(descriptors2)
                descriptors1 = descriptors1.type(torch.float)
                descriptors2 = descriptors2.type(torch.float)
                # reshape labels
                labels = torch.from_numpy(labels).type(torch.float)
                lables = labels.squeeze()
                labels = labels.unsqueeze(1)

                # tensor to kNN Graph
                g1 = dgl.knn_graph(pts1, k)
                g2 = dgl.knn_graph(pts2, k)
                print(k, "-NN Graphs")
                print("g1 nodes: ", g1.number_of_nodes(), "| edges: ", g1.number_of_edges())
                print("g2 nodes: ", g2.number_of_nodes(), "| edges: ", g2.number_of_edges())
                print()
                if (len(g2) != len(descriptors2)):  #
                    continue                        # fix for DGL kNN module bug -> skip if condition is met

                ## FORWARD PASS 1 ## FEATURE NETWORK ##
                # get abstract, lower dimensional representation of nodes for each set of input with a forward pass
                nodeRep1 = net1(g1, descriptors1)
                nodeRep2 = net2(g2, descriptors2)
                # merge node representation
                nodePairRep = TensorHelper.nodeRepresentationMerge(nodeRep1, nodeRep2)

                ## FORWARD PASS 2 ## METRIC NETWORK ##
                pred = netClass(nodePairRep)
                pred = pred

                ## LOSS & BACKWARDS PASS ##
                loss = criterion(pred, labels)
                total_loss += loss.item()
                print('Epoch %d | Testing Loss: %.4f' % (epoch, loss.item()))


                ###########
                # Metrics #
                ###########

                ### Prepare data for metrics ###
                labels = labels.detach().numpy().flatten()
                pred = pred.detach().numpy().flatten()

                # AUC ROC  #
                ############
                # calculate roc curve
                fpr, tpr, thresholds = roc_curve(lables, pred)
                tprs.append(interp(mean_fpr, fpr, tpr))
                # calculate
                auc1 = roc_auc_score(labels, pred)
                if (count > 0):
                    mean_auc, std_auc = additionalMetrics.aucMeanAndStd(tprs, mean_fpr, aucs)
                else:
                    mean_auc, std_auc = 0, 0
                print('AUC: %.3f' % auc1, '| mean: %.3f' % mean_auc, '| std: %.3f' % std_auc)

                additionalMetrics.plotROC(labels, pred)

                # ROC with variance
                tprs[-1][0] = 0.0
                roc_auc = auc(fpr, tpr)
                aucs.append(roc_auc)
                plt.plot(fpr, tpr, lw=1, alpha=0.3,
                         label='ROC sample %d (AUC = %0.2f)' % (count, roc_auc))

                if (count % 10 == 0):
                    additionalMetrics.plotROCMeanAndStD(tprs, mean_fpr, aucs, plt)

                ## Experiment tracking  ##
                ##########################
                total_loss += loss.item()
                experiment.log_metric("batch_train_loss", loss.item())
                experiment.log_metric("batch_train_total_loss", total_loss)
                experiment.log_metric("AUC", auc1)
                experiment.log_figure("ROC Curve", plt)

                row = [(str)(count), (str)(epoch), (str)(loss.item()), (str)(total_loss),
                       (str)(time.datetime.now()),
                       (str)(auc1), (str)(mean_auc), (str)(std_auc)]
                with open('Results/training_results.csv', 'a') as csvFile:
                    writer = csv.writer(csvFile)
                    writer.writerow(row)
                csvFile.close()

                # Thresholding #
                ################
                for i in range(len(pred)):
                    # Thresholding @ 0.0
                    if (pred[i] > 0.0):
                        pred[i] = 1
                    else:
                        pred[i] = 0

                # confusion matrix
                # data = {'pred': labels, 'labels': pred}
                # df = pd.DataFrame(data, columns=['labels', 'pred'])
                # confusion_matrix = pd.crosstab(df['labels'], df['pred'], rownames=['labels'], colnames=['pred'])
                # print(confusion_matrix)

                print()
                count += 1