from data_generator import dataSaver

if __name__ == '__main__':
    dataSaver.saveTrainData(10, 300) #1st arg = number of images in folder | 2nd arg = number of variations of each image
    dataSaver.saveTestData(10, 100)